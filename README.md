A grab-site Docker image by JAA

Intended for use without a gs-server, so the WebSocket connection is disabled entirely.

    docker build -t grab-site-jaa:latest .
    docker run --rm -d -v $(pwd)/data:/data:rw grab-site-jaa:latest --1 https://example.org/

# Tor
This image includes support for grabbing through Tor:

1. Create a Docker bridge network for connecting to an isolated Tor proxy image: `docker network create --driver bridge --subnet 10.91.50.0/24 tor`
2. Run a Tor proxy in that network with a specified IP, e.g. `docker run -d --restart=always --name tor-socks-proxy --network tor --ip 10.91.50.2 peterdavehello/tor-socks-proxy:latest`
3. Run this image while specifying the relevant environment variables: `docker run --rm -d -v $(pwd)/data:/data:rw -e TORSOCKS_TOR_ADDRESS=10.91.50.2 -e TORSOCKS_TOR_PORT=9150 --network tor grab-site-jaa:latest --1 https://check.torproject.org/`
