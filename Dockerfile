ARG PYTHON_VERSION=3.7
ARG ALPINE_VERSION=3.14

# Build torsocks from source for the syscall fixes
# Use Alpine 3.13 for compatibility with old Docker daemons (https://wiki.alpinelinux.org/wiki/Draft_Release_Notes_for_Alpine_3.14.0#faccessat2)
FROM alpine:3.13 AS torsocks_build
ARG TORSOCKS_VERSION=f9721f38aa78bcd249237c1777fde0e6743351ce
RUN apk add --no-cache autoconf automake build-base gcc git libc-dev libtool make \
	&& git clone https://gitlab.torproject.org/tpo/core/torsocks.git /torsocks \
	&& cd /torsocks \
	&& git checkout ${TORSOCKS_VERSION} \
	&& ./autogen.sh \
	&& ./configure \
	&& make \
	&& chmod 755 src/bin/torsocks

# Based on https://github.com/ArchiveTeam/grab-site/pull/195
FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

ARG GRAB_SITE_VERSION=2.2.3

WORKDIR /app
VOLUME [ "/data" ]

RUN apk add --no-cache \
		git \
		gcc \
		libxml2-dev \
		musl-dev \
		libxslt-dev \
		g++ \
		re2-dev \
		libffi-dev \
		openssl-dev \
		patch \
	&& ln -s /usr/include/libxml2/libxml /usr/include/libxml \
	&& addgroup -S grab-site \
	&& adduser -S -G grab-site grab-site \
	&& chown -R grab-site:grab-site $(pwd) \
	&& mkdir -p /data \
	&& chown -R grab-site:grab-site /data

COPY --from=torsocks_build /torsocks/src/bin/torsocks /usr/local/bin/torsocks
COPY --from=torsocks_build /torsocks/src/lib/.libs/*.a /usr/local/lib/torsocks/
COPY --from=torsocks_build /torsocks/src/lib/.libs/*.so* /usr/local/lib/torsocks/
COPY --from=torsocks_build /torsocks/doc/torsocks.conf /usr/local/etc/tor/torsocks.conf

USER grab-site:grab-site
COPY --chown=grab-site:grab-site *.patch /tmp/grab-site-patches/
RUN git clone https://github.com/ArchiveTeam/grab-site.git /app \
	&& git checkout ${GRAB_SITE_VERSION} \
	&& git apply --verbose /tmp/grab-site-patches/*.patch \
	&& pip install --no-cache-dir . \
	&& rm -rf /tmp/grab-site-patches
COPY --chown=grab-site:grab-site entrypoint-jaa /app/entrypoint-jaa
ENV PATH="/app:$PATH"
ENTRYPOINT ["entrypoint-jaa"]

WORKDIR /data
